package s59160561.informatics.buu.supercarproject

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import s59160561.informatics.buu.supercarproject.database.CarDatabase
import s59160561.informatics.buu.supercarproject.database.CarDatabaseDAO
import s59160561.informatics.buu.supercarproject.database.CarDatabaseModels
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class TestCar {

    private lateinit var sleepDao: CarDatabaseDAO
    private lateinit var db: CarDatabase

    @Before
    fun createDb() {

        val context = InstrumentationRegistry.getInstrumentation().targetContext
        db = Room.inMemoryDatabaseBuilder(context, CarDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        sleepDao = db.carDatabaseDAO()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun insertAndGetBrand() {
        val todo = CarDatabaseModels( "testBrand","testModel","testPic","testDetail")
        sleepDao.insert(todo)
        val tonight = sleepDao.findByBrand(todo.carBrand)
        Assert.assertEquals(tonight?.carBrand, todo.carBrand)
    }
}