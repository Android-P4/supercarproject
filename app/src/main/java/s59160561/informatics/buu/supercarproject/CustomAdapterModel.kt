package s59160561.informatics.buu.supercarproject

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

class CustomAdapterModel(var context: Context?, var carModel: ArrayList<CarModel>) : BaseAdapter() {


    private class ViewHolder(row: View?) {
        var txtName: TextView

        init {
            this.txtName = row?.findViewById(R.id.txtModels) as TextView
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view: View?
        var viewHolder: ViewHolder
        if (convertView == null) {
            var layout = LayoutInflater.from(context)
            view = layout.inflate(R.layout.list_car_model, parent, false)
            viewHolder = ViewHolder(view)
            view.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }
        var car: CarModel = getItem(position) as CarModel
        viewHolder.txtName.text = car.name

        return view as View

    }

    override fun getItem(position: Int): Any {
        return carModel.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return carModel.count();
    }

    fun remove(position: Int) {
        carModel.removeAt(position)
    }

}