package s59160561.informatics.buu.supercarproject


import android.Manifest
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.pm.PackageManager
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.core.app.ShareCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import s59160561.informatics.buu.supercarproject.database.CarDatabaseModels
import s59160561.informatics.buu.supercarproject.database.FavoriteDatabaseModels
import s59160561.informatics.buu.supercarproject.databinding.FragmentCarDetailBinding


/**
 * A simple [Fragment] subclass.
 */
class CarDetailFragment : Fragment() {

    private lateinit var binding: FragmentCarDetailBinding
    private lateinit var insertViewModel: InsertCarFragmentModel
    private lateinit var insertFavViewModel: InsertFavoriteFragmentModel
    private lateinit var btnClose: Button
    private lateinit var btnSave: Button
    private var menuStar: Menu? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_car_detail, container, false
        );
        if (activity?.applicationContext?.let {
                ContextCompat.checkSelfPermission(
                    it,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            } != PackageManager.PERMISSION_GRANTED) {
            val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            requestPermissions(permissions, 1001)
        }
        setHasOptionsMenu(true)
        insertViewModel = ViewModelProviders.of(this).get(InsertCarFragmentModel::class.java)
        insertFavViewModel =
            ViewModelProviders.of(this).get(InsertFavoriteFragmentModel::class.java)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        var model = arguments?.getString("model")
        (activity as AppCompatActivity).supportActionBar?.title = model
        setData(model)
        setIconStar(model)
    }

    private fun setIconStar(model: String?) {
        insertFavViewModel.allFav.observe(this, Observer { t ->
            t.forEach {
                if (it.carModel == model) {
                    menuStar?.getItem(1)?.icon = activity?.applicationContext?.let { it ->
                        ContextCompat.getDrawable(
                            it, android.R.drawable.btn_star_big_on
                        )
                    }
                }
            }
        })
    }

    private fun setData(model: String?) {
        val mapImage = setImage()
        val mapText = setTextDetail()
        val settingsDialog = Dialog(activity)
        var imageBox: ImageView
        var saveImageBtn: Button
        var nameModelOne = model + "1"
        var nameModelTwo = model + "2"
        var nameModelThree = model + "3"
        try {
            setShowImage(mapImage, nameModelOne, nameModelTwo, nameModelThree)
            setShowDetail(mapText, model + "")
            binding.carDetailImg1.setOnClickListener {
                settingsDialog.setContentView(layoutInflater.inflate(R.layout.image_layout, null))
                setButton(settingsDialog)
                imageBox = settingsDialog.findViewById(R.id.imageViewDialog)
                saveImageBtn = settingsDialog.findViewById(R.id.btnSaveImage)
                imageBox.setImageResource(mapImage.get(nameModelOne)!!)
                saveImageToGallery(saveImageBtn, mapImage, nameModelOne)
                settingsDialog.show()

            }
            binding.carDetailImg2.setOnClickListener {
                settingsDialog.setContentView(layoutInflater.inflate(R.layout.image_layout, null))
                setButton(settingsDialog)
                imageBox = settingsDialog.findViewById(R.id.imageViewDialog)
                saveImageBtn = settingsDialog.findViewById(R.id.btnSaveImage)
                imageBox.setImageResource(mapImage.get(nameModelTwo)!!)
                saveImageToGallery(saveImageBtn, mapImage, nameModelTwo)
                settingsDialog.show()
            }
            binding.carDetailImg3.setOnClickListener {
                settingsDialog.setContentView(layoutInflater.inflate(R.layout.image_layout, null))
                setButton(settingsDialog)
                imageBox = settingsDialog.findViewById(R.id.imageViewDialog)
                saveImageBtn = settingsDialog.findViewById(R.id.btnSaveImage)
                imageBox.setImageResource(mapImage.get(nameModelThree)!!)
                saveImageToGallery(saveImageBtn, mapImage, nameModelThree)
                settingsDialog.show()
            }
        } catch (e: KotlinNullPointerException) {
            Log.e("error", "Not a Picture")
        }

    }

    private fun saveImageToGallery(
        saveImageBtn: Button,
        mapImage: HashMap<String, Int>,
        nameModelOne: String
    ) {
        saveImageBtn.setOnClickListener {
            it.isEnabled = false
            val uri = saveImage(mapImage.get(nameModelOne)!!, nameModelOne)
            Toast.makeText(getActivity(), "saved : $uri", Toast.LENGTH_SHORT).show()
        }
    }

    private fun saveImage(drawable: Int, title: String): Uri {
        val drawable =
            activity?.applicationContext?.let { ContextCompat.getDrawable(it, drawable) }

        val bitmap = (drawable as BitmapDrawable).bitmap

        val savedImageURL = MediaStore.Images.Media.insertImage(
            activity?.contentResolver,
            bitmap,
            title,
            "Image of $title"
        )

        return Uri.parse(savedImageURL)
    }

    private fun setShowImage(
        map: HashMap<String, Int>,
        nameModelOne: String,
        nameModelTwo: String,
        nameModelThree: String
    ) {
        binding.carDetailImg1.setImageResource(map.get(nameModelOne)!!)
        binding.carDetailImg2.setImageResource(map.get(nameModelTwo)!!)
        binding.carDetailImg3.setImageResource(map.get(nameModelThree)!!)
    }

    private fun setShowDetail(
        map: HashMap<String, Int>,
        text: String
    ) {
        binding.txtDetails.setText(map.get(text)!!)
    }

    private fun setImage(): HashMap<String, Int> {
        val map = HashMap<String, Int>()
        map["CL Type-S1"] = R.drawable.cltypes1
        map["CL Type-S2"] = R.drawable.cltypes2
        map["CL Type-S3"] = R.drawable.cltypes3
        map["NSX (1995-2005)1"] = R.drawable.nsx19951
        map["NSX (1995-2005)2"] = R.drawable.nsx19952
        map["NSX (1995-2005)3"] = R.drawable.nsx19953
        map["NSX (2006-present)1"] = R.drawable.nsx20161
        map["NSX (2006-present)2"] = R.drawable.nsx20162
        map["NSX (2006-present)3"] = R.drawable.nsx20163
        map["Valkyrie1"] = R.drawable.valkyrie1
        map["Valkyrie2"] = R.drawable.valkyrie2
        map["Valkyrie3"] = R.drawable.valkyrie3
        map["Vanquish Zagato1"] = R.drawable.vanquishzagato1
        map["Vanquish Zagato2"] = R.drawable.vanquishzagato2
        map["Vanquish Zagato3"] = R.drawable.vanquishzagato3
        map["DB111"] = R.drawable.db111
        map["DB112"] = R.drawable.db112
        map["DB113"] = R.drawable.db113
        map["R81"] = R.drawable.audir81
        map["R82"] = R.drawable.audir82
        map["R83"] = R.drawable.audir83
        map["RS61"] = R.drawable.rs61
        map["RS62"] = R.drawable.rs62
        map["RS63"] = R.drawable.rs63
        map["TT RS1"] = R.drawable.ttrs1
        map["TT RS2"] = R.drawable.ttrs2
        map["TT RS3"] = R.drawable.ttrs3
        map["Continental GT3-R1"] = R.drawable.continentalgt3r1
        map["Continental GT3-R2"] = R.drawable.continentalgt3r2
        map["Continental GT3-R3"] = R.drawable.continentalgt3r3
        map["Continental GT Z1"] = R.drawable.gtz1
        map["Continental GT Z2"] = R.drawable.gtz2
        map["Continental GT Z3"] = R.drawable.gtz3
        map["Continental Supersports1"] = R.drawable.supersports1
        map["Continental Supersports2"] = R.drawable.supersports2
        map["Continental Supersports3"] = R.drawable.supersports3
        return map
    }

    private fun setTextDetail(): HashMap<String, Int> {
        val map = HashMap<String, Int>()
        map["CL Type-S"] = R.string.txtDetailCl
        map["NSX (1995-2005)"] = R.string.txtDetailsNsx1996
        map["NSX (2006-present)"] = R.string.txtDetailsNsx2016
        map["Valkyrie"] = R.string.txtDetailValkyrie
        map["Vanquish Zagato"] = R.string.txtDetailVanquish
        map["DB11"] = R.string.txtDetailDB11
        map["R8"] = R.string.txtDetailR8
        map["RS6"] = R.string.txtDetailRS6
        map["TT RS"] = R.string.txtDetailTTRS
        map["Continental GT3-R"] = R.string.txtDetailGT3R
        map["Continental GT Z"] = R.string.txtDetailGTZ
        map["Continental Supersports"] =  R.string.txtDetailSupersports
        return map
    }

    private fun setButton(settingsDialog: Dialog) {
        btnClose = settingsDialog.findViewById(R.id.btnClose)
        btnSave = settingsDialog.findViewById(R.id.btnSaveImage)

        btnClose.setOnClickListener {
            settingsDialog.dismiss()
        }
        btnSave.setOnClickListener {

        }
    }

    private fun onFavorite() {
        var model = arguments?.getString("model")
        val fav = FavoriteDatabaseModels(model!!)
        insertFavViewModel.insert(fav)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (menu != null) {
            if (inflater != null) {
                super.onCreateOptionsMenu(menu, inflater)
            }
        }
        inflater?.inflate(R.menu.detail_menu, menu)
        menu.getItem(0).setVisible(false);
        menu.getItem(2).setVisible(false);
        this.menuStar = menu;
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.favoriteMenuButton -> onFavorite()
        }
        return super.onOptionsItemSelected(item)
    }


}
