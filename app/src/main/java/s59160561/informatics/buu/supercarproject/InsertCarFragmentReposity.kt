package s59160561.informatics.buu.supercarproject

import androidx.lifecycle.LiveData
import s59160561.informatics.buu.supercarproject.database.CarDatabaseDAO
import s59160561.informatics.buu.supercarproject.database.CarDatabaseModels
import s59160561.informatics.buu.supercarproject.database.FavoriteDatabaseDAO
import s59160561.informatics.buu.supercarproject.database.FavoriteDatabaseModels

class InsertCarFragmentReposity(private val carDao: CarDatabaseDAO) {


    val allWords: LiveData<List<CarDatabaseModels>> = carDao.getAllCar()


    fun insert(car: CarDatabaseModels) {
        carDao.insert(car)
    }

    fun clear(item:String) {
        carDao.clear(item)
    }



}

class InsertFavoriteFragmentReposity(private val favDao: FavoriteDatabaseDAO) {
    val allFavorite: LiveData<List<FavoriteDatabaseModels>> = favDao.getAllFavorite()

    fun insert(fav: FavoriteDatabaseModels) {
        favDao.insertFavorite(fav)
    }
    fun clear(){
        favDao.clearAllFavorite()
    }
    fun deleteFavorite(item: String){
        favDao.clearFavorite(item);
    }
}
