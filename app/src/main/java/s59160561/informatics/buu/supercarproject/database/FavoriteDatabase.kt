package s59160561.informatics.buu.supercarproject.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(FavoriteDatabaseModels::class), version = 1, exportSchema = false)
abstract class FavDatabase : RoomDatabase() {

    abstract fun favoriteDatabaseDAO(): FavoriteDatabaseDAO


    companion object {
        @Volatile
        private var INSTANCE: FavDatabase? = null

        fun getDatabase(
            context: Context
        ): FavDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    FavDatabase::class.java,
                    "favorite_database"
                ).allowMainThreadQueries()
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}