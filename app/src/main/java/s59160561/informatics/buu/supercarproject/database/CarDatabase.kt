package s59160561.informatics.buu.supercarproject.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = arrayOf(CarDatabaseModels::class), version = 1, exportSchema = false)
abstract class CarDatabase : RoomDatabase() {

    abstract fun carDatabaseDAO(): CarDatabaseDAO


    companion object {
        @Volatile
        private var INSTANCE: CarDatabase? = null

        fun getDatabase(
            context: Context
        ): CarDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    CarDatabase::class.java,
                    "car_database"
                ).allowMainThreadQueries()
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}

