package s59160561.informatics.buu.supercarproject


import android.Manifest
import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.fragment_insert_car.*
import s59160561.informatics.buu.supercarproject.database.CarDatabaseModels
import s59160561.informatics.buu.supercarproject.databinding.FragmentInsertCarBinding
import java.util.*
import kotlin.collections.HashMap


/**
 * A simple [Fragment] subclass.
 */
class InsertCarFragment : Fragment() {
    private lateinit var binding: FragmentInsertCarBinding
    private lateinit var insertViewModel: InsertCarFragmentModel
    private val PICK_IMAGE_REQUEST = 71
    private var filePath: Uri? = null
    private var firebaseStore: FirebaseStorage? = null
    private var storageReference: StorageReference? = null
    var mAuth = FirebaseAuth.getInstance()
    var picUrl = "";

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        firebaseStore = FirebaseStorage.getInstance()
        storageReference = FirebaseStorage.getInstance().getReference();
        //mAuth = FirebaseAuth.getInstance()
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_insert_car, container, false
        );
        signIn("59160561@go.buu.ac.th", "@ken7613498520")
        checkPermission()
        pickImage()
        onSave()

        insertViewModel = ViewModelProviders.of(this).get(InsertCarFragmentModel::class.java)
        //insertViewModel.clear();

        return binding.root
    }

    private fun signIn(email: String, password: String) {

        Log.e("Auth", "Auth........")
        mAuth.signInWithEmailAndPassword(email, password)
            ?.addOnCompleteListener { task: Task<AuthResult> ->
                if (task.isSuccessful) {
//                var intent = Intent(getActivity(), InsertCarFragment::class.java)
//                intent.putExtra("id", mAuth.currentUser?.email)
//                startActivity(intent)
                    val firebaseUser = this.mAuth.currentUser!!

                } else {

                    Log.e("error", "${task.exception?.message}")
                }
            }
    }

    private fun checkPermission() {
        if (getActivity()?.applicationContext?.let {
                ContextCompat.checkSelfPermission(
                    it,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
            } != PackageManager.PERMISSION_GRANTED) {
            val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
            requestPermissions(permissions, PERMISSION_CODE);

        }
    }


    private fun onSave() {
        binding.btninsertsave.setOnClickListener {
            uploadImage()
            //Log.i("insert", insertbrand.toString())
            var txtBrand = insertbrand.text.toString()
            var txtModel = insertmodel.text.toString()
            var txtDetail = insertdetailMulti.text.toString()
            if (!txtBrand.isEmpty() && !txtModel.isEmpty() && !txtDetail.isEmpty()) {
                Handler().postDelayed({
                val word =
                    CarDatabaseModels(
                        txtBrand,
                        txtModel,
                        txtDetail,
                        picUrl
                    );
                    insertViewModel.insert(word)
                    Toast.makeText(getActivity(), "Insert success!!", Toast.LENGTH_LONG)
                        .show()
                    view?.findNavController()
                        ?.navigate(
                            InsertCarFragmentDirections.actionInsertCarFragmentToHomeFragment()
                        )
                }, 2000)
            } else {
                Toast.makeText(getActivity(), "Please fill out all information", Toast.LENGTH_LONG)
                    .show()
            }

        }
    }

    private fun pickImage() {
        binding.btnInsertImg.setOnClickListener {
            pickImageFromGallery()
        }
    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)

        Log.i("nameImage", filePath.toString())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        filePath = data?.data

        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            binding.btnInsertImg.setImageURI(data?.data)
        }
    }

    private fun addUploadRecordToDb(uri: String) {
        val db = FirebaseFirestore.getInstance()
        val data = HashMap<String, Any>()
        data["imageUrl"] = uri
        db.collection("posts")
            .add(data)
            .addOnSuccessListener { documentReference ->
                Toast.makeText(getActivity(), "Saved to DB", Toast.LENGTH_LONG).show()
            }
            .addOnFailureListener { e ->
                Toast.makeText(getActivity(), "Error saving to DB", Toast.LENGTH_LONG).show()
            }
    }

    private fun uploadImage() {
        if (filePath != null) {
            val ref = storageReference?.child("uploads/" + UUID.randomUUID().toString())
            val uploadTask = ref?.putFile(filePath!!)
            //Log.i("picture",uploadTask.toString())
            val urlTask =
                uploadTask?.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                    if (!task.isSuccessful) {
                        task.exception?.let {
                            throw it
                        }
                    }
                    return@Continuation ref.downloadUrl
                })?.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val downloadUri = task.result
                        Log.i("inside", downloadUri.toString())
                        picUrl = downloadUri.toString();
                        addUploadRecordToDb(downloadUri.toString())
                    } else {
                        // Handle failures
                    }
                }?.addOnFailureListener {

                }
        } else {
            Toast.makeText(getActivity(), "Please Upload an Image", Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        private val IMAGE_PICK_CODE = 1000;
        private val PERMISSION_CODE = 1001;
    }


}
