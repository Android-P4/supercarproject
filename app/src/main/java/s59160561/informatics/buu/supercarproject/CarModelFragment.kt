package s59160561.informatics.buu.supercarproject


import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.fragment_car_model.*
import s59160561.informatics.buu.supercarproject.databinding.FragmentCarModelBinding


/**
 * A simple [Fragment] subclass.
 */
class CarModelFragment : Fragment() {

    private lateinit var binding: FragmentCarModelBinding
    private lateinit var insertViewModel: InsertCarFragmentModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_car_model, container, false
        );
        insertViewModel = ViewModelProviders.of(this).get(InsertCarFragmentModel::class.java)
        checkModel()
        return binding.root
    }

    private fun checkModel() {
        var arrayModel: ArrayList<CarModel> = ArrayList()
        var model = arguments?.getString("brand")
        (activity as AppCompatActivity).supportActionBar?.title = model
        if (model == "Other") {
            var count = 0;
            Handler().postDelayed({
                insertViewModel.allWords.observe(this, Observer { car ->
                    car.forEach {
                        arrayModel.add(CarModel(it.carModel));
                        count++
                    }
                    if (count == car.size) {
                        listViewModels.adapter =
                            CustomAdapterModel(activity?.applicationContext, arrayModel);
                        setOnClickListView(arrayModel)
                    }
                })
            },200)
        } else {
            if (model == "Acura") {
                arrayModel.add(CarModel("CL Type-S"));
                arrayModel.add(CarModel("NSX (1995-2005)"));
                arrayModel.add(CarModel("NSX (2006-present)"));
            } else if (model == "Aston Martin") {
                arrayModel.add(CarModel("Valkyrie"));
                arrayModel.add(CarModel("Vanquish Zagato"));
                arrayModel.add(CarModel("DB11"));
            } else if (model == "Audi") {
                arrayModel.add(CarModel("R8"));
                arrayModel.add(CarModel("RS6"));
                arrayModel.add(CarModel("TT RS"));
            } else if (model == "Bently") {
                arrayModel.add(CarModel("Continental GT3-R"));
                arrayModel.add(CarModel("Continental GT Z"));
                arrayModel.add(CarModel("Continental Supersports"));
            }
            Handler().postDelayed({
                Log.i("brand" ,arrayModel[0].name)
                listViewModels.adapter =
                    CustomAdapterModel(getActivity()?.applicationContext, arrayModel);
                setOnClickListView(arrayModel)
            },200)
        }

    }

    private fun setOnClickListView(arrayModel: ArrayList<CarModel>) {
        listViewModels.setOnItemClickListener { parent, view, position, id ->
            var model = arguments?.getString("brand")
            if (model == "Other") {
                view.findNavController()
                    .navigate(CarModelFragmentDirections.actionCarModelFragmentToOtherFragment2(arrayModel[position].name))
            } else {
                view.findNavController()
                    .navigate(CarModelFragmentDirections.actionCarModelFragmentToCarDetailFragment(arrayModel[position].name))
            }
        }
    }
}
