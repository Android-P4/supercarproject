package s59160561.informatics.buu.supercarproject

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView


class CustomAdapterBrand(var context: Context?, var carbrand: ArrayList<CarBrand>): BaseAdapter() {
    private class ViewHolder(row: View?){
        var txtName: TextView
        var imgLogo: ImageView

        init {
            this.txtName = row?.findViewById(R.id.brand) as TextView
            this.imgLogo = row?.findViewById(R.id.logoPicture) as ImageView
        }
    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view: View?
        var viewHolder: ViewHolder
        if(convertView == null){
            var layout = LayoutInflater.from(context)
            view = layout.inflate(R.layout.list_caritem,parent,false)
            viewHolder = ViewHolder(view)
            view.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }
        var car:CarBrand = getItem(position) as CarBrand
        viewHolder.txtName.text = car.name
        viewHolder.imgLogo.setImageResource(car.image)
        return view as View

    }

    override fun getItem(position: Int): Any {
        return carbrand.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return carbrand.count();
    }

}