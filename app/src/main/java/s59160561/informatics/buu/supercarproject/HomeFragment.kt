package s59160561.informatics.buu.supercarproject

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.core.os.bundleOf

import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import s59160561.informatics.buu.supercarproject.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentHomeBinding>(
            inflater,
            R.layout.fragment_home, container, false
        )

        var array: ArrayList<CarBrand> = setBrand()

        binding.listViewData.adapter = CustomAdapterBrand(getActivity()?.applicationContext, array);

        binding.listViewData.setOnItemClickListener { parent, view, position, id ->
            view.findNavController()
                .navigate(
                    HomeFragmentDirections.actionHomeFragmentToCarModelFragment2(
                        array[position].name,
                        "brand"
                    )
                )
        }
        setHasOptionsMenu(true)
        return binding.root
    }

    private fun setBrand(): ArrayList<CarBrand> {
        var array: ArrayList<CarBrand> = ArrayList()
        array.add(CarBrand("Acura", R.drawable.acura));
        array.add(CarBrand("Aston Martin", R.drawable.aston_martin));
        array.add(CarBrand("Audi", R.drawable.audi));
        array.add(CarBrand("Bently", R.drawable.bently));
        array.add(CarBrand("Other", R.drawable.other));
        return array
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (menu != null) {
            if (inflater != null) {
                super.onCreateOptionsMenu(menu, inflater)
            }
        }
        inflater?.inflate(R.menu.options_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(
            item!!,
            view!!.findNavController()
        )
                || super.onOptionsItemSelected(item)
    }

}
