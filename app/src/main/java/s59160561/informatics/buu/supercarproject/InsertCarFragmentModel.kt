package s59160561.informatics.buu.supercarproject

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import s59160561.informatics.buu.supercarproject.database.*

class InsertCarFragmentModel(application: Application) : AndroidViewModel(application) {


    private val repository1: InsertCarFragmentReposity

    // LiveData gives us updated words when they change.
    val allWords: LiveData<List<CarDatabaseModels>>


    init {
        val wordsDao = CarDatabase.getDatabase(application.applicationContext).carDatabaseDAO()
        repository1 = InsertCarFragmentReposity(wordsDao)
        allWords = repository1.allWords

    }


    fun insert(word: CarDatabaseModels) = viewModelScope.launch {
        repository1.insert(word)
    }


    fun clear(item:String) = viewModelScope.launch {
        repository1.clear(item)
    }

}

class InsertFavoriteFragmentModel(application: Application) : AndroidViewModel(application) {
    private val repository: InsertFavoriteFragmentReposity

    val allFav: LiveData<List<FavoriteDatabaseModels>>

    init {
        val favsFao = FavDatabase.getDatabase(application.applicationContext).favoriteDatabaseDAO()
        repository = InsertFavoriteFragmentReposity(favsFao)
        allFav = repository.allFavorite
    }

    fun insert(fav: FavoriteDatabaseModels) = viewModelScope.launch {
        repository.insert(fav)
    }

    fun clear() = viewModelScope.launch {
        repository.clear()
    }
    fun deleteFavorite(item:String) = viewModelScope.launch {
        repository.deleteFavorite(item)
    }
}
