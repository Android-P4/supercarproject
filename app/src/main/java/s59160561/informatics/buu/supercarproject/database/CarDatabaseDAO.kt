package s59160561.informatics.buu.supercarproject.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface CarDatabaseDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(car: CarDatabaseModels)


    @Update
    fun update(car: CarDatabaseModels)

    //    @Query("SELECT * from car_all_items WHERE carId = :key")
//    fun get(key: Long): CarDatabaseModels?
//
    @Query("DELETE FROM car_all_items WHERE carModel = :carSelected ")
    fun clear(carSelected: String)

    @Query("SELECT * FROM car_all_items ORDER BY carBrand ASC")
    fun getAllCar(): LiveData<List<CarDatabaseModels>>


//    @Query("SELECT * FROM car_all_items ORDER BY carId DESC")
//    fun getAllCar(): LiveData<List<CarDatabaseModels>>
//    @Query("SELECT * from word_table ORDER BY word ASC")
//    fun getAllWords(): List<Word>


    @Query("SELECT * FROM car_all_items WHERE carBrand LIKE :title")
    fun findByBrand(title: String): CarDatabaseModels
}