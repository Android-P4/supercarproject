package s59160561.informatics.buu.supercarproject.database

import androidx.lifecycle.LiveData
import androidx.room.*


@Entity(tableName = "favorite_all_items")
data class FavoriteDatabaseModels(
    @PrimaryKey

    @ColumnInfo(name = "carModel")
    var carModel: String

)