package s59160561.informatics.buu.supercarproject


import android.app.Dialog
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.bumptech.glide.Glide
import s59160561.informatics.buu.supercarproject.database.CarDatabaseModels
import s59160561.informatics.buu.supercarproject.database.FavoriteDatabaseModels
import s59160561.informatics.buu.supercarproject.databinding.FragmentOtherBinding
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * A simple [Fragment] subclass.
 */
class OtherFragment : Fragment() {
    private lateinit var binding: FragmentOtherBinding
    private var menuStar: Menu? = null
    private lateinit var insertFavViewModel: InsertFavoriteFragmentModel
    private lateinit var insertViewModel: InsertCarFragmentModel
    private lateinit var btnClose: Button
    private lateinit var btnSave: Button
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_other, container, false
        );
        setHasOptionsMenu(true)
        insertFavViewModel =
            ViewModelProviders.of(this).get(InsertFavoriteFragmentModel::class.java)
        insertViewModel = ViewModelProviders.of(this).get(InsertCarFragmentModel::class.java)

        var model = arguments?.getString("model")
        setIconStar(model)
        (activity as AppCompatActivity).supportActionBar?.title = model
        setData(model)
        return binding.root
    }

    private fun setIconStar(model: String?) {
        insertFavViewModel.allFav.observe(this, Observer { t ->
            t.forEach {
                if (it.carModel == model) {
                    menuStar?.getItem(1)?.setIcon(getActivity()?.applicationContext?.let { it ->
                        ContextCompat.getDrawable(
                            it, android.R.drawable.btn_star_big_on
                        )
                    })
                }
            }
        })
    }

    private fun setData(model: String?) {
        var arrayModel: ArrayList<CarDatabaseModels> = ArrayList()
        var count = 0;
        Handler().postDelayed({
            insertViewModel.allWords.observe(this, Observer { t ->
                t.forEach {
                    if (it.carModel == model) {
                        arrayModel.add(
                            CarDatabaseModels(
                                it.carBrand,
                                it.carModel,
                                it.carDetail,
                                it.namePicture
                            )
                        );
                    }
                    count++;
                }
                if (count == t.size) {
                    showValue(arrayModel)
                }
            })
        },200)

    }

    private fun showValue(arrayModel: ArrayList<CarDatabaseModels>) {
        val settingsDialog = Dialog(getActivity())
        var imageBox: ImageView
        var saveImageBtn: Button
        //Log.i("url","${arrayModel[0].carDetail}")
        getActivity()?.applicationContext?.let {
            Glide.with(it)
                .load(arrayModel[0].namePicture)
                .into(binding.carDetailImg1)
        }
        binding.carDetailImg1.setOnClickListener {
            settingsDialog.setContentView(layoutInflater.inflate(R.layout.image_layout, null))
            setButton(settingsDialog)
            imageBox = settingsDialog.findViewById(R.id.imageViewDialog)
            getActivity()?.applicationContext?.let {
                Glide.with(it)
                    .load(arrayModel[0].namePicture)
                    .into(imageBox)
            }
            settingsDialog.show()
        }
        binding.txtShowOtherBrand.text = arrayModel[0].carBrand
        binding.txtShowOtherModel.text = arrayModel[0].carModel
        binding.txtDetails.text = arrayModel[0].carDetail
    }

    private fun setButton(settingsDialog: Dialog) {
        btnClose = settingsDialog.findViewById(R.id.btnClose)
        btnSave = settingsDialog.findViewById(R.id.btnSaveImage)

        btnClose.setOnClickListener {
            settingsDialog.dismiss()
        }
        btnSave.setVisibility(View.GONE)
    }

    private fun onFavorite() {
        var model = arguments?.getString("model")
        val fav = FavoriteDatabaseModels(model!!)
        insertFavViewModel.insert(fav)
    }

    private fun onDelete(){
        var model = arguments?.getString("model")
        if (model != null) {
            insertViewModel.clear(model)
            Toast.makeText(getActivity(), "Delete success.", Toast.LENGTH_LONG)
                .show()
            view?.findNavController()
                ?.navigate(
                    OtherFragmentDirections.actionOtherFragmentToHomeFragment()
                )
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (menu != null) {
            if (inflater != null) {
                super.onCreateOptionsMenu(menu, inflater)
            }
        }
        inflater?.inflate(R.menu.detail_menu, menu)
        menu.getItem(2).setVisible(false);
        this.menuStar = menu;
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.favoriteMenuButton -> onFavorite()
            R.id.deleteMenuButton -> onDelete()
        }
        return super.onOptionsItemSelected(item)
    }

}
