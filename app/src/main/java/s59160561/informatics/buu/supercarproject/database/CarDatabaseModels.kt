package s59160561.informatics.buu.supercarproject.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "car_all_items")
data class CarDatabaseModels(

    @PrimaryKey

    @ColumnInfo(name = "carBrand")
    val carBrand: String,

    @ColumnInfo(name = "carModel")
    var carModel: String,

    @ColumnInfo(name = "carDetail")
    var carDetail: String,

    @ColumnInfo(name = "namePicture")
    var namePicture: String


)
