package s59160561.informatics.buu.supercarproject.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface FavoriteDatabaseDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertFavorite(fav: FavoriteDatabaseModels)

    @Query("SELECT * FROM favorite_all_items ORDER BY carModel ASC")
    fun getAllFavorite(): LiveData<List<FavoriteDatabaseModels>>

    @Query("DELETE FROM favorite_all_items ")
    fun clearAllFavorite()

    @Query("DELETE FROM favorite_all_items WHERE carModel = :carSelected ")
    fun clearFavorite(carSelected: String)

}