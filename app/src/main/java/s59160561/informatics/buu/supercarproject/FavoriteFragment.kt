package s59160561.informatics.buu.supercarproject

import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.AdapterView
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.fragment_favorite.*
import s59160561.informatics.buu.supercarproject.databinding.FragmentFavoriteBinding
import com.hudomju.swipe.SwipeToDismissTouchListener
import com.hudomju.swipe.adapter.ListViewAdapter
import android.widget.TextView
import androidx.core.view.get


/**
 * A simple [Fragment] subclass.
 */
class FavoriteFragment : Fragment() {
    private lateinit var binding: FragmentFavoriteBinding
    private lateinit var insertFavViewModel: InsertFavoriteFragmentModel
    private var customAdapter: CustomAdapterModel? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_favorite, container, false
        );
        insertFavViewModel =
            ViewModelProviders.of(this).get(InsertFavoriteFragmentModel::class.java)
        setListView();
        return binding.root
    }

    private fun setSwipeDelete(arrayModel: ArrayList<CarModel>) {
        val touchListener = SwipeToDismissTouchListener(
            ListViewAdapter(listViewFavorite),
            object : SwipeToDismissTouchListener.DismissCallbacks<ListViewAdapter> {
                override fun canDismiss(position: Int): Boolean {
                    return true
                }

                override fun onDismiss(view: ListViewAdapter, position: Int) {
                    val ttd =
                        listViewFavorite.get(position).findViewById(R.id.txtModels) as TextView
                    Log.i("position", ttd.text.toString());
                    customAdapter?.remove(position)
                    insertFavViewModel.deleteFavorite(ttd.text.toString())
                    setListView()
                }
            })
        listViewFavorite!!.setOnTouchListener(touchListener)
        listViewFavorite!!.setOnScrollListener(touchListener.makeScrollListener() as AbsListView.OnScrollListener)
        listViewFavorite!!.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                if (touchListener.existPendingDismisses()) {
                    touchListener.undoPendingDismiss()
                } else {
                    var whoPage = checkModel(arrayModel[position].name)
                    Handler().postDelayed({
                        if (whoPage) {
                            view.findNavController()
                                .navigate(
                                    FavoriteFragmentDirections.actionFavoriteFragmentToCarDetailFragment(
                                        arrayModel[position].name
                                    )
                                )
                        } else {
                            view.findNavController()
                                .navigate(
                                    FavoriteFragmentDirections.actionFavoriteFragmentToOtherFragment(
                                        arrayModel[position].name
                                    )
                                )
                        }
                    }, 200)
                }
            }
    }

    private fun setListView() {
        var arrayModel: ArrayList<CarModel> = ArrayList()
        var count = 0;
        Handler().postDelayed({
            insertFavViewModel.allFav.observe(this, Observer { t ->
                t.forEach {
                    arrayModel.add(CarModel(it.carModel));
                    count++
                }
                if (count == t.size) {
                    listViewFavorite.adapter =
                        CustomAdapterModel(getActivity()?.applicationContext, arrayModel);
                    setSwipeDelete(arrayModel)
                }
            })
        },200)
    }

    private fun checkModel(model: String): Boolean {
        var arrayModelCheck: ArrayList<CarModel> = ArrayList();
        var check = false
        arrayModelCheck.add(CarModel("CL Type-S"));
        arrayModelCheck.add(CarModel("NSX (1995-2005)"));
        arrayModelCheck.add(CarModel("NSX (2006-present)"));
        arrayModelCheck.add(CarModel("Valkyrie"));
        arrayModelCheck.add(CarModel("Vanquish Zagato"));
        arrayModelCheck.add(CarModel("DB11"));
        arrayModelCheck.add(CarModel("R8"));
        arrayModelCheck.add(CarModel("RS6"));
        arrayModelCheck.add(CarModel("TT RS"));
        arrayModelCheck.add(CarModel("Continental GT3-R"));
        arrayModelCheck.add(CarModel("Continental GT Z"));
        arrayModelCheck.add(CarModel("Continental Supersports"));

        arrayModelCheck.forEach {
            if (it.name == model) {
                check = true
            }
        }
        return check
    }
}
